<?php

/**
 * @file na_arbitrator.module
 *
 * This module exists as a glue module between node_access and other modules
 * that want to use node_access. It controls the writing of records and
 * makes it possible for modules to work together by dealing with nodes
 * that other modules don't want to deal with.
 *
 * This is a core patch effort, but will start out as a module with full
 * intent of being moved into node.module as soon as this solution
 * is accepted.
 */

/**
 * Implementation of hook_help
 */
function na_arbitrator_help($section) {
  switch($section) {
    case 'admin/modules#description':
      return t('The node access arbitrator module makes it possible for compliant node access modules to co-exist. This module contains no user-serviceable parts.');
  }
}

/**
 * Implementation of hook_node_grants
 *
 * The arbitrator controls only one grant, the 'arbitrator' grant. When
 * moved to node.module, this will become the 'all' grant. By default,
 * all users automatically get this grant of grant ID 1.
 */
function na_arbitrator_node_grants($user, $op) {
  return array('arbitrator' => array(1));
}

/**
 * Implementation of hook_nodeapi
 *
 * When a node is saved, the arbitrator will ask any other security modules
 * for grants. If a grant list is returned, it will write that. If not,
 * it will write its own grant which gives every user view access.
 */
function na_arbitrator_nodeapi(&$node, $op, $teaser = NULL, $page = NULL) { 
  switch ($op) {
    case 'insert':
    case 'update':
      na_arbitrator_acquire_grants($node);
      break;
  }
}

/**
 * This function will call module invoke to get a list of grants and then
 * write them to the database.
 *
 * @param $node
 *   The $node to acquire grants for.
 */
function na_arbitrator_acquire_grants($node) {
  // Unfortunately hook_node_grants is misnamed. I think it should be
  // hook_node_user_grants.
  $grants = module_invoke_all('node_access_grants', $node);
  if (!$grants) {
    $grants[] = array('realm' => 'arbitrator', 'gid' => 1, 'grant_view' => 1, 'grant_update' => 0, 'grant_delete' => 0);
  }
  else {
    // retain grants by highest priority
    $grant_by_priority = array();
    foreach ($grants as $g) {
      $grant_by_priority[intval($g['priority'])][] = $g;
    }
    krsort($grant_by_priority);
    $grants = current($grant_by_priority);
  }

  na_arbitrator_write_grants($node, $grants);
}

/**
 * This function will write a list of grants to the database, deleting
 * any pre-existing grants. If a realm is provided, it will only
 * delete grants from that realm, but it will always delete a grant
 * from the 'arbitrator' realm. Modules which utilize node_access can
 * use this function when doing mass updates due to widespread permission
 * changes.
 *
 * @param $node
 *   The $node being written to. All that is necessary is that it contain a nid.
 * @param $grants
 *   A list of grants to write. Each grant is an array that must contain the
 *   following keys: realm, gid, grant_view, grant_update, grant_delete.
 *   The realm is specified by a particular module; the gid is as well, and
 *   is a module-defined id to define grant privileges. each grant_* field
 *   is a boolean value.
 * @param $realm
 *   If provided, only read/write grants for that realm.
 * @param $delete
 *   If false, do not delete records. This is only for optimization purposes,
 *   and assumes the caller has already performed a mass delete of some form.
 */
function na_arbitrator_write_grants($node, $grants, $realm = NULL, $delete = TRUE) {
  if ($delete) {
    $query = 'DELETE FROM {node_access} WHERE nid = %d';
    if ($realm) {
      $query .= " AND realm in ('%s', 'arbitrator')";
    }
    db_query($query, $node->nid, $realm);
  }

  // This optimization reduces the number of db inserts a little bit. We could
  // optimize further for mass updates if we wanted.
  $values = array();

  $query = '';
  foreach ($grants as $grant) {
    if ($realm && $realm != $grant['realm']) {
      continue;
    }
    $query .= ($query ? ', ' : '') . "(%d, '%s', %d, %d, %d, %d)";
    // I'm not sure += works right here; if it does we should use that instead.
    $values[] = $node->nid;
    $values[] = $grant['realm'];
    $values[] = $grant['gid'];
    $values[] = $grant['grant_view'];
    $values[] = $grant['grant_update'];
    $values[] = $grant['grant_delete'];
  }

  $query = "INSERT INTO {node_access} (nid, realm, gid, grant_view, grant_update, grant_delete) VALUES " . $query;
  
  db_query($query, $values);
}

/**
 * implementation of hook_settings
 */
function na_arbitrator_settings() {
  $form['markup'] = array(
    '#value' => t('Click submit to reset your access database; this may be necessary right after installing na_arbitrator or a module that relies on na_arbitrator. NOTE: If you are using a node_access module that does not use the na_arbitrator API, its access control records will be destroyed! If you are even the slightest bit unsure, please back up your database BEFORE pressing submit here! YOU CANNOT UNDO THIS! This process may take some time to run.')
  );
  $form['#submit']['na_arbitrator_settings_submit'] = array('na_arbitrator_settings_submit' => array());
  return $form;
}

/**
 * reset the node access database
 */
function na_arbitrator_settings_submit() {
  db_query("DELETE FROM {node_access}");
  $result = db_query("SELECT nid FROM {node}");
  while ($node = db_fetch_object($result)) {
    na_arbitrator_acquire_grants(node_load($node->nid));
  }
}
