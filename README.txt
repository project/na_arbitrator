This package is currently usable. The _access modules provided with
this setup are technically demonstration modules; they are there to
provide a template for developers to create their own _access modules
that can work well together.

That said, they are both functional modules and they should work.
Hopefully, there will be more.

Installing this package is easy: Drop the entire directory in your
Drupal modules directory and turn it on.

You may need to go to admin/settings/na_arbitrator and 'reset' so
that existing content gets the proper access controls.
