Creating an na_arbitrator based module consists of implementing two hooks, 
and keeping an eye out for conditions that can change globally. The
forum access and workflow access modules are both good examples of this.
For very simple access, you only need to do two things.

1) Implement the Drupal hook, hook_node_grants().

This hook must return an array of arrays in this form:
array(
  'realm' => array(
    grant_id,
    grant_id,
    grant_id,
  ),
);

A node grant is telling the system that the user has some kind of membership
privileges. Many modules simply return an array of the roles that a user
is a member of; the ACL.module will return all access control lists that
a user is a member of. Very simple modules simply use 1 grant_id that
indicates the user is a member of the realm. A good example of this is a
module that does some sort of 'over 18' check to ensure content is
visible. The grant id can mean *anything the realm wants it to mean* and
all that matters, in the end, is that users are either members of that
realm + gid or they are not; if they are members, this function tells the
node access system about it.

2) Implement the na_arbitrator hook hook_node_access_grants($node)

This hook returns an array of permissions that the module cares about. If it
doesn't care about the node, it can return NULL. The permissions arrays are
in this form:

array(
  array(
    'realm' => ...,
    'gid' => ...,
    'grant_view' => 0/1,
    'grant_update' => 0/1,
    'grant_delete' => 0/1,
  )
)

That's the minimum. Now, the clever will note that this basically updates the
node permissions whenever a node is saved. Sometimes there are global changes
that will change a lot of things (such as changing general permissions on a
list that a node is a member of--such as taxonomy access rules, or workflow
access rules). In that case, the module is responsible for updating all of
those nodes. Here's an example:

    // mass update
    $result = db_query("SELECT n.nid FROM {node} n LEFT JOIN {workflow_node} wn ON wn.nid = n.nid WHERE wn.sid = %d", $sid);
    while ($node = db_fetch_object($result)) {
      na_arbitrator_acquire_grants(node_load($node->nid), $grants, 'workflow_access');
    }
  }

This bit of code gets all the nodes that need to be updated, and forces the 
arbitrator to update it. Yes, this could be a time intensive process. But
it should also be a relatively rare occurrence. It's a downside to the system
that we have to live with.